
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { Model } = require('../user');
const config = require('../config').config(process.env.NODE_ENV);

const authentication = (user) => {
    const response = {};

    const payload = {
        user: {
            email: user.email,
        },
    };

    const token = jwt.sign(
        payload,
        config.jwt.secret, {
            expiresIn: '2d',
        },
    );
    if (!token) {
        response.status = 500;
        response.error = 'Internal server error';
    } else {
        response.status = 200;
        response.token = token;
        response.user = payload.user;
    }

    return response;
};

exports.login = async (req, res) => {
    const { email, password } = req.body;

    await Model.findOne({ email })
        .then((user) => {
            if (!user) {
                return res.status(404).send({ status: 404, error: 'User not found' });
            }

            const response = bcrypt.compare(password, user.password, (error, isMatch) => {
                if (error) {
                    return res.status(500).send({ status: 500, error });
                }
                if (!isMatch) {
                    return res.status(401).send({ status: 401, error: 'Authentication error' });
                }

                return authentication(user);
            });

            return res.status(response.status).send(response);
        })
        .catch((error) => { res.status(500).send({ status: 500, error }); });
};

exports.register = async (req, res) => {
    const { username, password, email } = req.body;
    const result = {};

    try {
        const user = await Model.findOne({
            $or: [
                { email },
                { username },
            ],
        });

        if (user) {
            return res.staus(409).send({ status: 409, error: 'User Already Exists' });
        }

        const newUser = new Model(req.body);

        const salt = await bcrypt.genSalt(10);
        newUser.password = await bcrypt.hash(password, salt);

        await newUser.save();

        const payload = {
            user: {
                email,
            },
        };

        jwt.sign(
            payload,
            config.jwt.secret, {
                expiresIn: '2d',
            }, (error, token) => {
                if (error) {
                    throw error;
                }

                result.status = 200;
                result.token = token;
                result.user = payload.user;
            },
        );
    } catch (e) {
        return res.status(500).send({ status: 500, error: e.message });
    }

    return res.status(200).send(result);
};
