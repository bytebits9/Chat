const mongoose = require('mongoose');
const logger = require('../logger');

exports.init = () => {
    const dbUri = 'mongodb://127.0.0.1:27017/spacechat';
    mongoose.Promise = global.Promise;

    mongoose.connect(dbUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
        .catch((error) => {
            if (error) {
                logger.log('error', error);
            }
        });
};
