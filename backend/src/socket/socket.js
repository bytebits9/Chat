const http = require('http');
const socketIo = require('socket.io');
const logger = require('../logger');

const events = (io) => {
    io.on('connection', (socket) => {
        logger.log('info', `${socket.id} conncted.`);

        socket.on('newMessage', (message) => {
            socket.broadcast.emit('addMessage', message);

            logger.log('info', `${message.message}`);
        });

        socket.on('disconnected', (user) => {
            logger.log('info', `${user.id} disconnected`);
        });
    });
};
module.exports.init = (app) => {
    const server = http.createServer(app);
    const socket = socketIo(server);

    events(socket);

    return server;
};
