const socket = require('./socket');

module.exports = {
    socket: socket.init,
};
