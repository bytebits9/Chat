module.exports.index = [
    (req, res) => {
        res.render('index', {
            title: 'SpaceChat',
        });
    },
];
