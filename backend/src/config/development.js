const development = {
    app: {
        port: 8080,
    },
    db: {
        host: 'localhost',
        port: '27017',
        name: 'spacechat',
    },
    jwt: {
        secret: 'development',
    },
};

module.exports = development;
