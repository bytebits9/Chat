const router = require('express').Router();
const user = require('../user');

router.get('/', user.getAll);
router.get('/:id', user.getById);
router.post('/', user.insert);
router.put('/:id', user.update);
router.delete('/:id', user.delete);

module.exports = router;
