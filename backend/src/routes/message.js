const router = require('express').Router();
const message = require('../message');

router.get('/', message.getAll);
router.get('/:id', message.getById);
router.post('/', message.insert);
router.put('/:id', message.update);
router.delete('/:id', message.delete);

module.exports = router;
