const Message = require('./model');

module.exports.getAll = async (req, res) => {
    const payload = req.decode;
    if (payload) {
        await Message.find()
            .then((messages) => {
                res.status(200).send({ status: 200, result: messages });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.getById = async (req, res) => {
    const { id } = req.params;

    if (req.decode) {
        await Message.findById(id)
            .then((message) => {
                res.status(200).send({ status: 200, result: message });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.update = async (req, res) => {
    const { id } = req.params;

    if (req.decode) {
        await Message.findByIdAndUpdate(id, req.body)
            .then((success) => {
                res.status(201).send({ status: 201, result: success });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.insert = async (req, res) => {
    const { message } = req.body;

    if (req.decode) {
        const messageData = {
            message,
            created_on: Date.now(),
        };

        const newMessage = new Message(messageData);
        await newMessage.save()
            .then((success) => {
                res.status(201).send({ status: 201, result: success });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.delete = async (req, res) => {
    const { id } = req.params;

    if (req.decode) {
        await Message.findByIdAndDelete(id)
            .then((success) => {
                res.status(202).send({ status: 202, result: success });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};
