const mongoose = require('mongoose');
const bcrypt = require('bcrypt');


const UserSchema = new mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    created_on: { type: Date, required: true },
});

UserSchema.pre('save', (next) => {
    bcrypt.genSalt(10, (error, salt) => {
        if (error) {
            next();
        }
        bcrypt.hash(this.password, salt, (err, hash) => {
            if (err) {
                next();
            }

            this.password = hash;
        });
    });

    if (!this.created_on) {
        this.created_on = Date().now();
    }
});

module.exports = mongoose.model('User', UserSchema);
