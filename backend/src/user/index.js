const user = require('./user');
const Model = require('./model');

module.exports = {
    getAll: user.getAll,
    getById: user.getById,
    insert: user.insert,
    update: user.update,
    delete: user.delete,
    Model,
};
