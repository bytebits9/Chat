const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');

const app = express();

// Components
const config = require('./src/config').config(process.env.NODE_ENV);
const logger = require('./src/logger');
const mongodb = require('./src/mongodb');
const server = require('./src/socket').socket(app);
const routes = require('./src/routes');

mongodb.initialize();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(routes);

server.listen(config.app.port, () => {
    logger.log('info', `Server listening on: ${server.address().address}:${server.address().port}`);
});
