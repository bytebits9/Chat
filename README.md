# SpaceChat
A real time chat application built with Node.js and Socket.io.

## Getting started

### Requirements

- Node.js
- MongoDB
- Redis

## Installing

#### Installing

1. Clone SpaceChat repository
```git clone https://github.com/FFx0q/Chat.git```
2. Enter repository folder
```cd spaceChat/```
3. Install dependencies
```npm install```
4. In config file set info about mongodb & redis

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository]().

## License

This project is licensed under the General Public License version 3 - see the [LICENSE.md](LICENSE.md) file for details